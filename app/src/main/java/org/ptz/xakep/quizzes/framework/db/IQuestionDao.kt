package org.ptz.xakep.quizzes.framework.db

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query

@Dao
interface IQuestionDao {
    @Query("SELECT * FROM Questions WHERE quizId = :quizId")
    suspend fun getQuestionsByQuizId(quizId: Int): Array<QuestionEntity>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertQuestions(vararg question: QuestionEntity)

    @Query("SELECT id, correctAnswerId FROM Questions WHERE quizId = :quizId")
    suspend fun getCorrectAnswers(quizId: Int): Array<CorrectAnswer>

    // sets correct answer id and explanation
    @Query("UPDATE Questions SET correctAnswerId = :correctAnswerId, explanation = :explanation WHERE id = :questionId")
    suspend fun updateQuestion(
        questionId: Int,
        correctAnswerId: Int,
        explanation: String)
}