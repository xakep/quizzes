package org.ptz.xakep.quizzes.framework.db

class LessonListModel(
    val id: Int,
    val quizId: Int?,
    val title: String,
    val quizQuestionsCount: Int?,
    val score: Int,
    val gravatar: String
)