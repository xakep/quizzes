package org.ptz.xakep.quizzes.di

import org.koin.dsl.module
import org.ptz.xakep.quizzes.interactors.*

val interactors = module {
    factory { GetLessons(get()) }
    factory { GetLessonsCount(get()) }
    factory { LoadLessonList(get()) }
    factory { GetLessonById(get()) }
    factory { LoadLesson(get()) }
    factory { GetQuizById(get()) }
    factory { GetCorrectAnswersByQuizId(get()) }
    factory { LoadCorrectAnswers(get()) }
    factory { MarkAsSolved(get()) }
    factory { GetProgress(get()) }
    factory { RefreshLessonList(get()) }
    factory { RecordAttempt(get()) }
    factory { GetScoreByQuizId(get()) }
    factory { ParseLessonList(get()) }
}