package org.ptz.xakep.quizzes

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Handler
import android.view.View
import org.ptz.xakep.quizzes.presentation.splash.SplashViewModel
import org.koin.android.viewmodel.ext.android.viewModel
import org.ptz.xakep.quizzes.databinding.ActivitySplashBinding

class SplashActivity : AppCompatActivity() {

    private val viewModel: SplashViewModel by viewModel()
    private lateinit var binding: ActivitySplashBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivitySplashBinding.inflate(layoutInflater)
        val view = binding.root
        setContentView(view)

        viewModel.progress.observe(this) {
            if (it == -1) {
                binding.progressBar.visibility = View.GONE
                binding.errorLayout.root.visibility = View.VISIBLE
            } else {
                switchProgressBars()
                binding.listLoadingProgress.progress = it
                binding.progressTextView.text = String.format(getString(R.string.progress_percent), it)
            }
        }

        viewModel.done.observe(this) {
            openMainActivity()
        }

        binding.errorLayout.noConnTryAgainButton.setOnClickListener {
            binding.errorLayout.root.visibility = View.GONE
            binding.progressBar.visibility = View.VISIBLE
            viewModel.load(this.getString(R.string.lessons_list_url))
        }

        viewModel.load(this.getString(R.string.lessons_list_url))
    }

    private fun switchProgressBars() {
        binding.progressBar.visibility = View.GONE
        binding.listLoadingProgress.visibility = View.VISIBLE
    }

    private fun openMainActivity(delay: Long = 0) {
        Handler(mainLooper).postDelayed({
            // Start activity
            startActivity(Intent(this, MainActivity::class.java))

            // Animate the loading of new activity
            overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out)

            // Close this activity
            finish()
        }, delay)
    }
}
