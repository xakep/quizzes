package org.ptz.xakep.quizzes.presentation.splash

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.flowOn
import kotlinx.coroutines.launch
import org.ptz.xakep.quizzes.interactors.GetLessonsCount
import org.ptz.xakep.quizzes.interactors.LoadLessonList
import org.ptz.xakep.quizzes.interactors.ParseLessonList

class SplashViewModel(
    private val getLessonsCount: GetLessonsCount,
    private val loadLessonList: LoadLessonList,
    private val parseLessonList: ParseLessonList
) : ViewModel() {

    val progress: MutableLiveData<Int> = MutableLiveData()
    val done: MutableLiveData<Unit> = MutableLiveData()

    fun load(url: String) {
        viewModelScope.launch {
            val count = getLessonsCount()
            if (count == 0) {
                val lessonListAsString = loadLessonList(url)
                if (lessonListAsString.isEmpty()) {
                    progress.postValue(-1) // error
                    return@launch
                }

                parseLessonList(lessonListAsString)
                    .flowOn(Dispatchers.Main)
                    .collect {
                        progress.postValue(it)
                        if (it == 100) {
                            done.postValue(Unit)
                        }
                    }
            } else {
                done.postValue(Unit)
            }
        }
    }
}