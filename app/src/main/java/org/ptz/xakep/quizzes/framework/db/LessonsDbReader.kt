package org.ptz.xakep.quizzes.framework.db

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase

@Database(
    entities = [
        LessonEntity::class,
        TeacherEntity::class,
        QuestionEntity::class,
        AnswerEntity::class,
        AttemptEntity::class,
        TopicNameEntity::class,
        TopicEntity::class
    ],
    version = 1,
    exportSchema = false
)
abstract class LessonsDbReader : RoomDatabase() {
    companion object {

        private const val DATABASE_NAME = "quizzes.db"

        private var instance: LessonsDbReader? = null

        private fun create(context: Context): LessonsDbReader =
            Room.databaseBuilder(context, LessonsDbReader::class.java, DATABASE_NAME)
                .fallbackToDestructiveMigration()
                .build()

        fun getInstance(context: Context): LessonsDbReader =
            (instance ?: create(context)).also { instance = it }
    }

    abstract fun lessonsDao(): ILessonDao

    abstract fun teachersDao(): ITeacherDao

    abstract fun questionsDao(): IQuestionDao

    abstract fun answersDao(): IAnswerDao

    abstract fun attemptsDao(): IAttemptsDao

    abstract fun topicsDao(): ITopicDao
}