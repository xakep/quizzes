package org.ptz.xakep.quizzes.framework.db

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "TopicNames")
data class TopicNameEntity(
    @PrimaryKey(autoGenerate = false) val id: Int,
    @ColumnInfo(name = "topicName") val topicName: String
)