package org.ptz.xakep.quizzes.presentation.filters

import android.app.Dialog
import android.content.Context
import android.view.Gravity
import android.view.ViewGroup
import android.widget.Button
import android.widget.EditText
import android.widget.RadioGroup
import androidx.core.view.children
import com.google.android.material.button.MaterialButtonToggleGroup
import org.ptz.xakep.quizzes.R
import org.ptz.xakep.quizzes.domain.Filter
import org.ptz.xakep.quizzes.domain.LessonType

class FiltersDialog(
    context: Context,
    applyFunc: (filter: Filter) -> Unit,
    private val dialog: Dialog = Dialog(context, R.style.MaterialDialogSheet)) {

    var filter: Filter = Filter.Default

    init {
        dialog.setContentView(R.layout.filters_dialog)
        dialog.window?.setGravity(Gravity.TOP)
        dialog.window?.setLayout(
            ViewGroup.LayoutParams.MATCH_PARENT,
            ViewGroup.LayoutParams.WRAP_CONTENT
        )
        dialog.setCanceledOnTouchOutside(false)

        val cancelBtn: Button = dialog.findViewById(R.id.cancelFiltersBtn)
        cancelBtn.setOnClickListener {
            dialog.hide()
        }

        val toggleGroup: MaterialButtonToggleGroup = dialog.findViewById(R.id.toggleButton)
        val searchEditText: EditText = dialog.findViewById(R.id.filterSearchEditText)
        val difficultyRadioButtons: RadioGroup = dialog.findViewById(R.id.difficultyRadioButtons)
        val applyBtn: Button = dialog.findViewById(R.id.applyFiltersBtn)
        applyBtn.setOnClickListener {
            val lessonType = when (toggleGroup.checkedButtonId) {
                R.id.filterSolvedBtn -> LessonType.SOLVED
                R.id.filterAttemptedBtn -> LessonType.ATTEMPTED
                R.id.filterNoQuizBtn -> LessonType.NOQUIZ
                else -> LessonType.TODO
            }

            val difficulty = difficultyRadioButtons.children.find {
                it.id == difficultyRadioButtons.checkedRadioButtonId
            }?.tag.toString().toInt()
            filter = Filter(lessonType, searchEditText.text.toString().trim(), difficulty)
            applyFunc(filter)
            dialog.hide()
        }
    }

    fun show() {
        dialog.show()
    }
}