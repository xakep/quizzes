package org.ptz.xakep.quizzes.framework.db

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.Index
import androidx.room.PrimaryKey

@Entity(tableName = "Questions", foreignKeys = [androidx.room.ForeignKey(
    entity = LessonEntity::class,
    parentColumns = arrayOf("quizId"),
    childColumns = arrayOf("quizId"),
    onDelete = androidx.room.ForeignKey.CASCADE
)], indices = [Index(value = ["quizId"])])
class QuestionEntity(
    @PrimaryKey(autoGenerate = false) val id: Int,
    @ColumnInfo(name = "quizId") val quizId: Int,
    @ColumnInfo(name = "question") val question: String,
    @ColumnInfo(name = "correctAnswerId") val correctAnswerId: Int?,
    @ColumnInfo(name = "explanation") val explanation: String?
)