package org.ptz.xakep.quizzes.presentation.quiz

import android.graphics.Color
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.RadioButton
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import org.ptz.xakep.quizzes.QuizActivity
import org.ptz.xakep.quizzes.R
import org.ptz.xakep.quizzes.UIUtils
import org.ptz.xakep.quizzes.databinding.QuestionBinding
import org.ptz.xakep.quizzes.domain.Question

class QuizAdapter: ListAdapter<Question, QuizAdapter.ViewHolder>(QuestionDiffCallback()) {
    private var highlightCorrectAnswers: Boolean = false

    class ViewHolder(val binding: QuestionBinding) : RecyclerView.ViewHolder(binding.root)

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val binding = QuestionBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return ViewHolder(binding)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) = holder.run {
        holder.binding.questionText.text =
            UIUtils.convertToHtml(currentList[position].text.replace("_", "_____"))
        holder.binding.explanationText.visibility = View.GONE
        enableFinishBtn(holder)
        setAnswers(holder, currentList[position])

        holder.binding.quizAnswers.setOnCheckedChangeListener{ _, checkedId ->
            if (checkedId != -1) {
                currentList[position].setAnswer(checkedId)
                enableFinishBtn(holder)
            }
        }
    }

    private fun enableFinishBtn(holder: ViewHolder) {
        (holder.binding.quizAnswers.context as QuizActivity)
            .enableFinishButton(!currentList.any { it.selectedAnswer == -1 }) // TODO
    }

    fun resetAnswers() {
        currentList.forEach {
            it.setAnswer(-1)
        }
    }

    fun showCorrectAnswers(givenAnswers: Map<Int, Int>) {
        currentList.forEach {
            it.setAnswer(givenAnswers[it.id] ?: -1)
        }
    }

    fun setHighlightAnswers(value: Boolean) {
        highlightCorrectAnswers = value
    }

    private fun setAnswers(holder: ViewHolder, question: Question) {
        holder.binding.quizAnswers.removeAllViews()
        holder.binding.quizAnswers.setOnCheckedChangeListener(null)

        val ctx = holder.binding.quizAnswers.context
        for (answer in question.answers) {
            val rb = LayoutInflater.from(ctx)
                .inflate(R.layout.quiz_option_item, holder.binding.quizAnswers, false) as RadioButton

            if (question.selectedAnswer != -1 && question.selectedAnswer == answer.id) {
                rb.isChecked = true
            }

            rb.id = answer.id
            rb.text = answer.text
            if (highlightCorrectAnswers && question.correctAnswerId == answer.id) {
                rb.setBackgroundColor(Color.CYAN)
            }

            holder.binding.quizAnswers.addView(rb)
        }

        if (highlightCorrectAnswers) {
            if (question.explanation != "") {
                holder.binding.explanationText.text = question.explanation
                holder.binding.explanationText.visibility = View.VISIBLE
            } else {
                holder.binding.explanationText.visibility = View.GONE
            }
        }
    }
}