package org.ptz.xakep.quizzes.presentation.lessons

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import org.ptz.xakep.quizzes.domain.ListLesson
import org.ptz.xakep.quizzes.domain.Filter
import org.ptz.xakep.quizzes.interactors.GetLessons
import org.ptz.xakep.quizzes.interactors.RefreshLessonList

class LessonsViewModel(
    private val getLessons: GetLessons,
    private val refreshLessonList: RefreshLessonList
) : ViewModel() {

    val lessons: MutableLiveData<List<ListLesson>> = MutableLiveData()
    val newQuizzesFound: MutableLiveData<Int> = MutableLiveData()

    fun load(filter: Filter = Filter.Default) {
        viewModelScope.launch(Dispatchers.Default) {
            lessons.postValue(getLessons(filter))
        }
    }

    fun refresh(url: String) {
        viewModelScope.launch(Dispatchers.Default) {
            newQuizzesFound.postValue(refreshLessonList(url))
        }
    }
}