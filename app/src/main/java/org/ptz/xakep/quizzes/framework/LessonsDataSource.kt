package org.ptz.xakep.quizzes.framework

import androidx.sqlite.db.SimpleSQLiteQuery
import org.ptz.xakep.quizzes.data.ILessonsDataSource
import org.ptz.xakep.quizzes.domain.*
import org.ptz.xakep.quizzes.framework.db.*
import org.ptz.xakep.quizzes.remote.model.Teacher
import org.ptz.xakep.quizzes.remote.model.TopicName

class LessonsDataSource (
    private val lessonsDao: ILessonDao,
    private val teachersDao: ITeacherDao,
    private val questionsDao: IQuestionDao,
    private val answersDao: IAnswerDao,
    private val topicsDao: ITopicDao) : ILessonsDataSource {
    override suspend fun updateLesson(
        lessonId: Int,
        lesson: org.ptz.xakep.quizzes.remote.model.Lesson,
        gravatar: String?) {
        lessonsDao.updateLesson(
            lessonId,
            lesson.quizId ?: -lessonId,
            lesson.youtubeCode,
            lesson.description,
            lesson.questions.size)

        // update teacher name if needed
        if (gravatar != null) {
            teachersDao.setTeacherName(gravatar, lesson.teacherName)
        }

        // if lesson has a quiz, insert it
        if (lesson.quizId != null) {
            val questions = lesson.questions.map {
                QuestionEntity(it.id, lesson.quizId!!, it.text, null, null)
            }

            if (questions.isNotEmpty()) {
                questionsDao.insertQuestions(*questions.toTypedArray())

                val answers = mutableListOf<AnswerEntity>()
                // insert answers
                for (question in lesson.questions) {
                    answers.addAll(question.answers.map {
                        AnswerEntity(it.id, question.id, it.text)
                    })
                }

                answersDao.insertAnswers(*answers.toTypedArray())
            }
        }
    }

    override suspend fun markAsSolved(quizId: Int) {
        lessonsDao.markAsSolved(quizId)
    }

    override suspend fun getProgress(): Progress {
        val progress = lessonsDao.getProgress()

        return Progress(
            progress.solvedCount,
            progress.totalCount)
    }

    override suspend fun getLatestLesson(): LatestLesson {
        return lessonsDao.getLatestQuizLink()
    }

    override suspend fun getTopicNames(): List<TopicName> {
        return topicsDao.getTopicNames()
    }

    override suspend fun getTeachers(): List<Teacher> {
        return teachersDao.getTeachers()
    }

    override suspend fun getLessonById(id: Int): Lesson {
        val topicNames = topicsDao.getTopicNames(id)
        val lessonEntity = lessonsDao.getLessonById(id)

        return Lesson(
            lessonEntity.quizId ?: 0,
            lessonEntity.link,
            lessonEntity.youtube ?: "",
            lessonEntity.title,
            lessonEntity.difficulty,
            topicNames,
            lessonEntity.description ?: "",
            lessonEntity.quizQuestionsCount ?: -1,
            lessonEntity.teacherName,
            lessonEntity.teacherGravatar
        )
    }

    override suspend fun getLessonsCount(): Int {
        return lessonsDao.getLessonsCount()
    }

    override suspend fun getLessons(filter: Filter): List<ListLesson> {
        val queries = mapOf(
            LessonType.TODO to "SELECT id, quizId, title, quizQuestionsCount, -1 AS score, gravatar FROM LessonList LL join Teachers T on LL.teacherId = T.teacherId WHERE ((quizId IS NULL OR quizId != -1) AND solved = 0 %s %s) ORDER BY id DESC",
            LessonType.ATTEMPTED to "SELECT id, quizId, title, quizQuestionsCount, (SELECT highestScore FROM Attempts WHERE quizId = LL.quizId) AS score, gravatar FROM LessonList LL join Teachers T on LL.teacherId = T.teacherId WHERE solved = 0 AND quizId IN (SELECT quizId FROM Questions WHERE correctAnswerId IS NOT NULL GROUP BY quizId) %s %s ORDER BY id DESC",
            LessonType.SOLVED to "SELECT id, quizId, title, quizQuestionsCount, (SELECT highestScore FROM Attempts WHERE quizId = LL.quizId) AS score, gravatar FROM LessonList LL join Teachers T on LL.teacherId = T.teacherId WHERE solved = 1 %s %s ORDER BY id DESC",
            LessonType.NOQUIZ to "SELECT id, quizId, title, 0, -1 AS score, gravatar FROM LessonList LL join Teachers T on LL.teacherId = T.teacherId WHERE quizId < 0 %s %s ORDER BY id DESC"
        )
        var difficulty = "1,2,3,4,6,7" // no filters
        if (filter.difficulty != 0) {
            difficulty = filter.difficulty.toString()
        }
        var search = "" // no filters
        if (filter.search != "") {
            search = "AND title LIKE '%${filter.search}%'"
        }

        val queryAsString = queries[filter.lessonType] ?: ""
        val formattedQuery = String.format(queryAsString, "AND difficulty in (${difficulty})", search)

        return lessonsDao.getLessons(SimpleSQLiteQuery(formattedQuery)).map {
            ListLesson(
                it.id,
                it.title,
                it.quizQuestionsCount,
                it.score,
                it.gravatar // TODO: teacherGravatar
            )
        }
    }

    override fun insertLessons(
        topicNames: List<TopicName>,
        vararg listPageParsedLessons: ListPageParsedLesson) {
        val uniqueTeachers = listPageParsedLessons.map { it.teacher }.distinct()
        val teachers = uniqueTeachers
            .map {
                TeacherEntity(
                    it.teacherId,
                    it.name,
                    it.gravatar
                )
            }
        teachersDao.insertTeachers(*teachers.toTypedArray())

        if (topicNames.isNotEmpty()) {
            val topicNameEntities = topicNames.map {
                TopicNameEntity(it.id, it.name)
            }
            topicsDao.insertTopicNames(*topicNameEntities.toTypedArray())
        }

        val lessonEntities = listPageParsedLessons.map {
            LessonEntity(
                it.lessonId,
                null,
                it.link,
                null,
                it.title,
                it.teacher.teacherId,
                it.difficulty,
                false,
                null,
                null)
        }

        lessonsDao.insertLessons(*lessonEntities.toTypedArray())

        val topics = mutableListOf<TopicEntity>()
        listPageParsedLessons.forEach {
            it.topics.forEach { topicNameId ->
                topics.add(TopicEntity(0, it.lessonId, topicNameId))
            }
        }
        topicsDao.insertTopics(*topics.toTypedArray())
    }
}