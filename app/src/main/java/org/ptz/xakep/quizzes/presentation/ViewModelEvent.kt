package org.ptz.xakep.quizzes.presentation

data class ViewModelEvent<out T>(val status: Status, val data: T?, val error: Error?) {

    companion object {
        fun <T> pending(): ViewModelEvent<T> {
            return ViewModelEvent(Status.PENDING, null, null)
        }

        fun <T> loading(): ViewModelEvent<T> {
            return ViewModelEvent(Status.LOADING, null, null)
        }

        fun <T> success(data: T?): ViewModelEvent<T> {
            return ViewModelEvent(Status.SUCCESS, data, null)
        }

        fun <T> error(error: Error?): ViewModelEvent<T> {
            return ViewModelEvent(Status.ERROR, null, error)
        }
    }
}

enum class Status {
    SUCCESS,
    ERROR,
    LOADING,
    PENDING
}