package org.ptz.xakep.quizzes.presentation.lesson

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import kotlinx.coroutines.launch
import org.ptz.xakep.quizzes.domain.Lesson
import org.ptz.xakep.quizzes.interactors.GetLessonById
import org.ptz.xakep.quizzes.interactors.LoadLesson
import org.ptz.xakep.quizzes.presentation.ViewModelEvent

class LessonViewModel(
    private val getLessonById: GetLessonById,
    private val loadLesson: LoadLesson
) : ViewModel() {
    val state: MutableLiveData<ViewModelEvent<Int>> = MutableLiveData()
    val lesson: MutableLiveData<Lesson> = MutableLiveData()

    init {
        state.postValue(ViewModelEvent.pending())
    }

    fun getLesson(id: Int) {
        state.postValue(ViewModelEvent.loading())

        viewModelScope.launch {
            var model = getLessonById(id)
            if (model.quizQuestionsCount == -1) {
                val loaded = loadLesson(
                    id,
                    model.link,
                    if (model.teacherName.isEmpty()) model.teacherGravatar else null
                )
                if (loaded != null) {
                    model = loaded
                } else {
                    state.postValue(ViewModelEvent.error(Error()))
                    return@launch
                }
            }

            lesson.postValue(model)
        }
    }
}