package org.ptz.xakep.quizzes.framework.db

import androidx.room.*

@Entity(tableName = "LessonList", foreignKeys = [ForeignKey(
    entity = TeacherEntity::class,
    parentColumns = arrayOf("teacherId"),
    childColumns = arrayOf("teacherId"),
    onDelete = ForeignKey.CASCADE
)], indices = [Index(value = ["teacherId"]), Index(value = ["quizId"], unique = true)]
)
data class LessonEntity(
    @PrimaryKey(autoGenerate = false) val id: Int = 0,
    @ColumnInfo(name = "quizId") val quizId: Int?, // nullable
    @ColumnInfo(name = "link") val link: String, // url
    @ColumnInfo(name = "youtube") val youtube: String?, // nullable
    @ColumnInfo(name = "title") val title: String,
    @ColumnInfo(name = "teacherId") val teacherId: Int,
    @ColumnInfo(name = "difficulty") val difficulty: Int,
    @ColumnInfo(name = "solved") val solved: Boolean, // false by default
    @ColumnInfo(name = "description") val description: String?, // nullable
    @ColumnInfo(name = "quizQuestionsCount") val quizQuestionsCount: Int? // nullable
)