package org.ptz.xakep.quizzes

import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import org.ptz.xakep.quizzes.databinding.ActivityMainBinding
import org.ptz.xakep.quizzes.presentation.filters.FiltersDialog
import org.ptz.xakep.quizzes.presentation.lessons.LessonListFragment

class MainActivity : AppCompatActivity() {
    lateinit var filtersDialog: FiltersDialog
    private lateinit var binding: ActivityMainBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        val view = binding.root
        setContentView(view)

        val fragment = supportFragmentManager.findFragmentById(R.id.fragmentContainer) as LessonListFragment
        filtersDialog = FiltersDialog(this, fragment::applyFilters)

        binding.topAppBar.setOnMenuItemClickListener {
            when (it.itemId) {
                R.id.refreshItem -> {
                    binding.loadingProgressBar.visibility = View.VISIBLE
                    fragment.refreshLessonList {
                        binding.loadingProgressBar.visibility = View.GONE
                    }

                    true
                }
                R.id.filtersItem -> {
                    filtersDialog.show()
                    true
                }
                else -> false
            }
        }
    }
}
