package org.ptz.xakep.quizzes.framework.db

class ProgressModel(
    val solvedCount: Int,
    val totalCount: Int
)