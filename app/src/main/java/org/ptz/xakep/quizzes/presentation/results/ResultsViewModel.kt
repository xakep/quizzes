package org.ptz.xakep.quizzes.presentation.results

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import kotlinx.coroutines.launch
import org.ptz.xakep.quizzes.interactors.*
import org.ptz.xakep.quizzes.presentation.models.QuizGivenAnswers

class ResultsViewModel(
    private val getCorrectAnswersByQuizId: GetCorrectAnswersByQuizId,
    private val loadCorrectAnswers: LoadCorrectAnswers,
    private val getScoreByQuizId: GetScoreByQuizId,
    private val recordAttempt: RecordAttempt,
    private val markAsSolved: MarkAsSolved
) : ViewModel() {

    val score: MutableLiveData<String> = MutableLiveData()
    val solved: MutableLiveData<Boolean> = MutableLiveData()

    fun getResults(quizId: Int, link: String, givenAnswers: QuizGivenAnswers) {
        viewModelScope.launch {
            var existingAnswers = getCorrectAnswersByQuizId(quizId)
            if (existingAnswers[0].correctAnswerId == null) {
                // download and update the db
                existingAnswers = loadCorrectAnswers(quizId, link, givenAnswers.answers)
                if (existingAnswers.isEmpty()) {
                    // network error
                    score.postValue("")
                    return@launch
                }
            }

            var correct = 0
            val map = givenAnswers.answers.toMap()
            existingAnswers.forEach {
                if (it.correctAnswerId == map[it.questionId])
                    correct++
            }

            // record attempt if higher or doesn't exist yet
            val currentScore = getScoreByQuizId(quizId)
            if (currentScore < correct) {
                recordAttempt(quizId, correct)
            }

            score.postValue("You got $correct correct out of ${map.count()}.")
        }
    }

    fun markQuizAsSolved(quizId: Int) {
        viewModelScope.launch {
            markAsSolved(quizId)
            solved.postValue(true)
        }
    }
}