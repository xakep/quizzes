package org.ptz.xakep.quizzes.presentation.lessons

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import org.koin.android.viewmodel.ext.android.viewModel
import org.ptz.xakep.quizzes.LessonActivity
import org.ptz.xakep.quizzes.MainActivity
import org.ptz.xakep.quizzes.R
import org.ptz.xakep.quizzes.databinding.FragmentLessonsListBinding
import org.ptz.xakep.quizzes.domain.Filter

class LessonListFragment : Fragment() {
    private val viewModel: LessonsViewModel by viewModel()
    private lateinit var onRefreshComplete: () -> Unit
    private var _binding: FragmentLessonsListBinding? = null
    private val binding get() = _binding!!

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        _binding = FragmentLessonsListBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        val adapter = LessonsAdapter()
        binding.lessonsRecyclerView.adapter = adapter
        adapter.setOnClickListener(View.OnClickListener {
            val intent = Intent(context, LessonActivity::class.java)
            intent.putExtra(getString(R.string.lesson_id_extra), it.tag as Int)
            startActivity(intent)
            activity?.overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out)
        })

        viewModel.lessons.observe(viewLifecycleOwner, Observer {
            adapter.submitList(it)
        })

        viewModel.newQuizzesFound.observe(viewLifecycleOwner, Observer {
            if (this::onRefreshComplete.isInitialized) {
                onRefreshComplete()
                val message: String
                if (it > 0) {
                    message = String.format(getString(R.string.new_quizzes_available), it)
                    // reload list if found
                    viewModel.load(getFilter())
                } else {
                    message = if (it == 0)
                        getString(R.string.no_new_quizzes_found)
                    else getString(R.string.no_connection_error)
                }

                Toast.makeText(activity, message, Toast.LENGTH_LONG).show()
            }
        })
    }

    override fun onStart() {
        super.onStart()
        viewModel.load(getFilter())
    }

    fun refreshLessonList(onComplete: () -> Unit) {
        onRefreshComplete = onComplete
        viewModel.refresh(getString(R.string.lessons_list_url))
    }

    fun applyFilters(filter: Filter) {
        viewModel.load(filter)
    }

    private fun getFilter(): Filter {
        return (activity as MainActivity).filtersDialog.filter
    }
}