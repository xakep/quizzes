package org.ptz.xakep.quizzes.di

import org.koin.dsl.module
import org.ptz.xakep.quizzes.remote.parsers.LessonListParser
import org.ptz.xakep.quizzes.remote.parsers.LessonParser
import org.ptz.xakep.quizzes.remote.parsers.QuizResultsParser

val parsers = module {
    factory { LessonListParser() }
    factory { LessonParser() }
    factory { QuizResultsParser() }
}