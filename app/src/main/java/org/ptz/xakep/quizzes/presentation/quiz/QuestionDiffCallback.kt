package org.ptz.xakep.quizzes.presentation.quiz

import androidx.recyclerview.widget.DiffUtil
import org.ptz.xakep.quizzes.domain.Question

class QuestionDiffCallback: DiffUtil.ItemCallback<Question>() {
    override fun areItemsTheSame(oldItem: Question, newItem: Question): Boolean {
        return oldItem.id == newItem.id
    }

    override fun areContentsTheSame(oldItem: Question, newItem: Question): Boolean {
        return oldItem == newItem
    }
}