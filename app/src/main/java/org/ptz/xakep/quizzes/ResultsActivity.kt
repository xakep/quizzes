package org.ptz.xakep.quizzes

import android.content.Intent
import android.os.Bundle
import android.view.View.GONE
import android.view.View.VISIBLE
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import org.koin.android.viewmodel.ext.android.viewModel
import org.ptz.xakep.quizzes.databinding.ActivityResultsBinding
import org.ptz.xakep.quizzes.presentation.models.QuizGivenAnswers
import org.ptz.xakep.quizzes.presentation.results.ResultsViewModel

class ResultsActivity : AppCompatActivity() {

    private val viewModel: ResultsViewModel by viewModel()
    private lateinit var binding: ActivityResultsBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityResultsBinding.inflate(layoutInflater)
        val view = binding.root
        setContentView(view)

        val quizId = intent.getIntExtra(getString(R.string.quiz_id_extra), -1)
        viewModel.getResults(
            quizId,
            intent.getStringExtra(getString(R.string.quiz_link_extra)) ?: "", // TODO
            intent.getParcelableExtra<QuizGivenAnswers>(getString(R.string.given_answers)) ?: QuizGivenAnswers(
                listOf())
        )

        viewModel.score.observe(this, Observer {
            if (it == "") {
                // error
                binding.resultsProgressBar.visibility = GONE
                binding.errorLayout.root.visibility = VISIBLE
            } else {
                binding.resultTextView.text = it
                binding.resultsProgressBar.visibility = GONE
                binding.contentLayout.visibility = VISIBLE
            }
        })

        viewModel.solved.observe(this, Observer {
            Toast.makeText(this, getString(R.string.marked_as_solved), Toast.LENGTH_LONG).show()
        })

        binding.tryAgainButton.setOnClickListener {
            startActivity(createQuizActivityIntent(quizId, false))
        }

        binding.seeAnswersButton.setOnClickListener {
            startActivity(createQuizActivityIntent(quizId, true))
        }

        binding.watchButton.setOnClickListener {
            val code = intent.getStringExtra(getString(R.string.youtube_code_extra))
            UIUtils.watchOnYoutube(
                applicationContext,
                "${getString(R.string.youtube_domain)}$code"
            )
        }

        binding.markAsSolvedButton.setOnClickListener {
            viewModel.markQuizAsSolved(intent.getIntExtra(getString(R.string.quiz_id_extra), -1))
        }

        binding.showLessonsListButton.setOnClickListener {
            startActivity(Intent(this, MainActivity::class.java))
        }

        binding.openStatsButton.setOnClickListener {
            startActivity(Intent(this, StatsActivity::class.java))
        }

        binding.errorLayout.noConnTryAgainButton.setOnClickListener {
            binding.errorLayout.root.visibility = GONE
            binding.resultsProgressBar.visibility = VISIBLE
            viewModel.getResults(
                intent.getIntExtra(getString(R.string.quiz_id_extra), -1),
                intent.getStringExtra(getString(R.string.quiz_link_extra)) ?: "", // TODO
                intent.getParcelableExtra<QuizGivenAnswers>(getString(R.string.given_answers)) ?: QuizGivenAnswers(
                    listOf())
            )
        }
    }

    private fun createQuizActivityIntent(quizId: Int, showAnswers: Boolean): Intent {
        val newIntent = Intent(this, QuizActivity::class.java)
        newIntent.putExtra(getString(R.string.quiz_id_extra), quizId)
        if (showAnswers) {
            newIntent.putExtra(getString(R.string.show_correct_answers_extra), showAnswers)
            newIntent.putExtra(
                getString(R.string.given_answers),
                intent.getParcelableExtra<QuizGivenAnswers>(getString(R.string.given_answers)))
        }

        return newIntent
    }
}
