package org.ptz.xakep.quizzes.framework.db

import androidx.room.*
import androidx.sqlite.db.SupportSQLiteQuery
import org.ptz.xakep.quizzes.domain.LatestLesson

@Dao
interface ILessonDao {
    @RawQuery
    suspend fun getLessons(query: SupportSQLiteQuery): List<LessonListModel>

    @Query("SELECT COUNT(id) FROM LessonList")
    suspend fun getLessonsCount(): Int

    @Query("SELECT quizId, link, t.name as teacherName, t.gravatar as teacherGravatar, youtube, title, description, quizQuestionsCount, difficulty FROM LessonList l INNER JOIN Teachers t ON l.teacherId = t.teacherId WHERE id = :id")
    suspend fun getLessonById(id: Int): LessonModel

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertLessons(vararg lessons: LessonEntity)

    @Query("UPDATE LessonList SET quizId = :quizId, youtube = :youtubeCode, description = :description, quizQuestionsCount = :quizQuestionsCount WHERE id = :lessonId")
    suspend fun updateLesson(
        lessonId: Int,
        quizId: Int?,
        youtubeCode: String,
        description: String,
        quizQuestionsCount: Int)

    @Query("UPDATE LessonList SET solved = 1 WHERE quizId = :quizId")
    suspend fun markAsSolved(quizId: Int)

    @Query("SELECT (SELECT COUNT(*) FROM LessonList WHERE solved = 1) as solvedCount, COUNT(*) as totalCount FROM LessonList WHERE quizId is NULL or quizId != -1")
    suspend fun getProgress(): ProgressModel

    @Query("SELECT id as lessonId, link FROM LessonList ORDER BY id DESC LIMIT 1")
    suspend fun getLatestQuizLink(): LatestLesson
}