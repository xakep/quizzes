package org.ptz.xakep.quizzes.di

import org.koin.android.ext.koin.androidContext
import org.koin.dsl.module
import org.ptz.xakep.quizzes.framework.db.LessonsDbReader

val roomModule = module {
    single { LessonsDbReader.getInstance(androidContext()) }
    single { get<LessonsDbReader>().lessonsDao() }
    single { get<LessonsDbReader>().teachersDao() }
    single { get<LessonsDbReader>().questionsDao() }
    single { get<LessonsDbReader>().answersDao() }
    single { get<LessonsDbReader>().attemptsDao() }
    single { get<LessonsDbReader>().topicsDao() }
}