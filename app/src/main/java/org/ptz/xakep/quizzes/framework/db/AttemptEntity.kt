package org.ptz.xakep.quizzes.framework.db

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "Attempts", foreignKeys = [androidx.room.ForeignKey(
    entity = LessonEntity::class,
    parentColumns = arrayOf("quizId"),
    childColumns = arrayOf("quizId"),
    onDelete = androidx.room.ForeignKey.CASCADE
)])
data class AttemptEntity(
    @PrimaryKey(autoGenerate = false) val quizId: Int,
    @ColumnInfo(name = "highestScore") val highestScore: Int
)