package org.ptz.xakep.quizzes

import android.app.Application
import org.koin.android.ext.koin.androidContext
import org.koin.android.ext.koin.androidLogger
import org.koin.core.context.startKoin
import org.ptz.xakep.quizzes.di.applicationModule
import org.ptz.xakep.quizzes.di.interactors
import org.ptz.xakep.quizzes.di.parsers
import org.ptz.xakep.quizzes.di.roomModule

class MyApp : Application() {
    override fun onCreate() {
        super.onCreate()
        startKoin{
            androidLogger()
            androidContext(this@MyApp)
            modules(listOf(applicationModule, roomModule, interactors, parsers))
        }
    }
}