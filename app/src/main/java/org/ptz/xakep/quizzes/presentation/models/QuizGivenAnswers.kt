package org.ptz.xakep.quizzes.presentation.models

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
class QuizGivenAnswers(val answers: List<Pair<Int, Int>>) : Parcelable