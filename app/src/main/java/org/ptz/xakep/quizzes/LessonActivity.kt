package org.ptz.xakep.quizzes

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.Gravity
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import android.widget.TextView
import androidx.constraintlayout.widget.ConstraintSet
import androidx.lifecycle.Observer
import com.bumptech.glide.Glide
import org.ptz.xakep.quizzes.presentation.lesson.LessonViewModel
import org.koin.android.viewmodel.ext.android.viewModel
import org.ptz.xakep.quizzes.databinding.ActivityLessonBinding
import org.ptz.xakep.quizzes.presentation.Status

class LessonActivity : AppCompatActivity() {

    private val viewModel: LessonViewModel by viewModel()
    private lateinit var difficultyLevelLayoutParams: LinearLayout.LayoutParams
    private lateinit var binding: ActivityLessonBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityLessonBinding.inflate(layoutInflater)
        val view = binding.root
        setContentView(view)

        difficultyLevelLayoutParams = LinearLayout.LayoutParams(
            ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT
        )
        difficultyLevelLayoutParams.gravity = Gravity.CENTER

        viewModel.state.observe(this, Observer {
            if (it.status == Status.PENDING) {
                viewModel.getLesson(intent.getIntExtra(getString(R.string.lesson_id_extra), -1))
            }

            if (it.status == Status.ERROR) {
                binding.lessonProgressBar.visibility = View.GONE
                binding.errorLayout.root.visibility = View.VISIBLE
            }
        })

        viewModel.lesson.observe(this, Observer {
            // set the gravatar
            Glide.with(this)
                .load("${getString(R.string.gravatar_domain)}${it.teacherGravatar}")
                .into(binding.avatarImage)

            binding.lessonProgressBar.visibility = View.GONE
            binding.lessonContentGroup.visibility = View.VISIBLE

            if (it.quizQuestionsCount > 0) {
                binding.takeTheQuizBtn.text = String.format(getString(R.string.take_quiz_btn), it.quizQuestionsCount)
                val quizId = it.quizId
                val link = it.link
                val youtubeCode = it.youtube

                binding.takeTheQuizBtn.setOnClickListener {
                    val intent = Intent(this, QuizActivity::class.java)
                    intent.putExtra(getString(R.string.quiz_id_extra), quizId)
                    intent.putExtra(getString(R.string.quiz_link_extra), link)
                    intent.putExtra(getString(R.string.youtube_code_extra), youtubeCode)
                    startActivity(intent)
                    overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out)
                }
            } else {
                binding.takeTheQuizBtn.text = getString(R.string.quizless_lesson)
                binding.takeTheQuizBtn.isEnabled = false
            }

            binding.teacherNameTextView.text = it.teacherName
            binding.lessonTitleTextView.text = UIUtils.convertToHtml(it.title)
            binding.lessonDescriptionTextView.text = UIUtils.convertToHtml(it.description)

            val difficultyNames = resources.getStringArray(R.array.listDifficulties)
            val difficulties = Utils.getDifficulties(it.difficulty)
            var previousTextView: TextView = binding.difficultyTextView0
            for (i in difficulties.indices) {
                if (i == 0) {
                    binding.difficultyTextView0.text = difficultyNames[difficulties[i]]
                } else {
                    previousTextView = addTextView(
                        difficultyNames[difficulties[i]],
                        previousTextView.id)
                }
            }

            if (it.topics.isNotEmpty()) {
                it.topics.forEach {topicName ->
                    previousTextView = addTextView(
                        topicName,
                        previousTextView.id)
                }
            }

            val youtubeCode = it.youtube
            binding.watchOnYoutubeBtn.setOnClickListener {
                UIUtils.watchOnYoutube(
                    applicationContext,
                    "${getString(R.string.youtube_domain)}$youtubeCode"
                )
            }
        })

        binding.errorLayout.noConnTryAgainButton.setOnClickListener {
            binding.errorLayout.root.visibility = View.GONE
            binding.lessonProgressBar.visibility = View.VISIBLE
            viewModel.getLesson(intent.getIntExtra(getString(R.string.lesson_id_extra), -1))
        }
    }

    private fun addTextView(text: String, prevViewId: Int): TextView {
        val textView = TextView(this)
        textView.id = View.generateViewId()
        textView.layoutParams = difficultyLevelLayoutParams
        textView.text = UIUtils.convertToHtml(text)
        binding.lessonCard.addView(textView)
        val ids = binding.lessonBarrier.referencedIds.toMutableList()
        ids.add(textView.id)
        binding.lessonBarrier.referencedIds = ids.toIntArray()

        val cs = ConstraintSet()
        cs.clone(binding.lessonCard)
        cs.connect(textView.id, ConstraintSet.START, R.id.lessonCard, ConstraintSet.START, 0)
        cs.connect(textView.id, ConstraintSet.TOP, prevViewId, ConstraintSet.BOTTOM, 0)
        cs.applyTo(binding.lessonCard)

        return textView
    }
}
