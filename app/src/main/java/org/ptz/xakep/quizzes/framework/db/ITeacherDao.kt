package org.ptz.xakep.quizzes.framework.db

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import org.ptz.xakep.quizzes.remote.model.Teacher

@Dao
interface ITeacherDao {
    @Insert(onConflict = OnConflictStrategy.IGNORE)
    fun insertTeachers(vararg teacher: TeacherEntity)

    @Query("UPDATE Teachers SET name = :name WHERE gravatar = :gravatar")
    suspend fun setTeacherName(gravatar: String, name: String)

    @Query("SELECT teacherId, name, gravatar FROM Teachers")
    suspend fun getTeachers(): List<Teacher>
}