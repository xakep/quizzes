package org.ptz.xakep.quizzes.framework.db

class LessonModel(
    val quizId: Int?,
    val link: String,
    val teacherName: String,
    val teacherGravatar: String,
    val title: String,
    val youtube: String?,
    val description: String?,
    val quizQuestionsCount: Int?,
    val difficulty: Int
)