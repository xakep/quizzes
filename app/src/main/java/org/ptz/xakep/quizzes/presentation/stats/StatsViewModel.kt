package org.ptz.xakep.quizzes.presentation.stats

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import kotlinx.coroutines.launch
import org.ptz.xakep.quizzes.domain.Progress
import org.ptz.xakep.quizzes.interactors.GetProgress

class StatsViewModel(
    private val getProgressUseCase: GetProgress
) : ViewModel() {

    private val _progress: MutableLiveData<Progress> = MutableLiveData()
    val progress: LiveData<Progress> = _progress

    fun getProgress() {
        viewModelScope.launch {
            _progress.value = getProgressUseCase()
        }
    }
}