package org.ptz.xakep.quizzes.framework.db

class CorrectAnswer(
    val id: Int, // questionId
    val correctAnswerId: Int?
)