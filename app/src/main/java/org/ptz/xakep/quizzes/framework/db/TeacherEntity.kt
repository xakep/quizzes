package org.ptz.xakep.quizzes.framework.db

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "Teachers")
data class TeacherEntity(
    @PrimaryKey(autoGenerate = false) val teacherId: Int,
    @ColumnInfo(name = "name") val name: String,
    @ColumnInfo(name = "gravatar") val gravatar: String
)