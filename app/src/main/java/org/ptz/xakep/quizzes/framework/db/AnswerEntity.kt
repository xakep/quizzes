package org.ptz.xakep.quizzes.framework.db

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.Index
import androidx.room.PrimaryKey

@Entity(tableName = "Answers", foreignKeys = [androidx.room.ForeignKey(
    entity = QuestionEntity::class,
    parentColumns = arrayOf("id"),
    childColumns = arrayOf("questionId"),
    onDelete = androidx.room.ForeignKey.CASCADE
)], indices = [Index(value = ["questionId"])])
class AnswerEntity(
    @PrimaryKey(autoGenerate = false) val id: Int, // answerId
    @ColumnInfo(name = "questionId") val questionId: Int,
    @ColumnInfo(name = "answer") val answer: String
)