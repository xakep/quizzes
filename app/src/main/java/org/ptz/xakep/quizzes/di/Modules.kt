package org.ptz.xakep.quizzes.di

import org.koin.android.ext.koin.androidContext
import org.koin.android.viewmodel.dsl.viewModel
import org.koin.dsl.module
import org.ptz.xakep.quizzes.R
import org.ptz.xakep.quizzes.data.ILessonsDataSource
import org.ptz.xakep.quizzes.data.IQuizzesDataSource
import org.ptz.xakep.quizzes.data.LessonsRepository
import org.ptz.xakep.quizzes.data.QuizzesRepository
import org.ptz.xakep.quizzes.framework.LessonsDataSource
import org.ptz.xakep.quizzes.framework.QuizzesDataSource
import org.ptz.xakep.quizzes.presentation.lessons.LessonsViewModel
import org.ptz.xakep.quizzes.presentation.lesson.LessonViewModel
import org.ptz.xakep.quizzes.presentation.quiz.QuizAdapter
import org.ptz.xakep.quizzes.presentation.quiz.QuizViewModel
import org.ptz.xakep.quizzes.presentation.results.ResultsViewModel
import org.ptz.xakep.quizzes.presentation.splash.SplashViewModel
import org.ptz.xakep.quizzes.presentation.stats.StatsViewModel
import org.ptz.xakep.quizzes.remote.NetworkService
import org.ptz.xakep.quizzes.remote.services.AnswersService
import org.ptz.xakep.quizzes.remote.services.LessonListService
import org.ptz.xakep.quizzes.remote.services.LessonService

val applicationModule = module(override = true) {
    single { NetworkService.getInstance(androidContext().getString(R.string.domain_url)) }

    factory<ILessonsDataSource> { LessonsDataSource(get(), get(), get(), get(), get()) }
    factory { LessonsRepository(get()) }

    factory<IQuizzesDataSource> { QuizzesDataSource(get(), get(), get()) }
    factory { QuizzesRepository(get()) }

    viewModel { LessonsViewModel(get(), get()) }
    viewModel { SplashViewModel(get(), get(), get()) }
    viewModel { LessonViewModel(get(), get()) }
    viewModel { QuizViewModel(get()) }
    viewModel { ResultsViewModel(get(), get(), get(), get(), get()) }
    viewModel { StatsViewModel(get()) }

    factory { LessonListService(get(), get(), get()) }
    factory { LessonService(get(), get(), get()) }
    factory { AnswersService(get(), get(), get()) }

    factory { QuizAdapter() }
}