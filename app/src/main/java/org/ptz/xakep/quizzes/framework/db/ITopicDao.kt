package org.ptz.xakep.quizzes.framework.db

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import org.ptz.xakep.quizzes.remote.model.TopicName

@Dao
interface ITopicDao {
    @Insert(onConflict = OnConflictStrategy.IGNORE)
    fun insertTopicNames(vararg topicName: TopicNameEntity)

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    fun insertTopics(vararg topics: TopicEntity)

    @Query("SELECT topicName FROM TopicNames tn JOIN Topics t ON t.topicNameId = tn.id WHERE t.lessonId = :lessonId")
    suspend fun getTopicNames(lessonId: Int): List<String>

    @Query("SELECT id, topicName as name FROM TopicNames")
    suspend fun getTopicNames(): List<TopicName>
}