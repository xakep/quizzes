package org.ptz.xakep.quizzes.framework.db

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query

@Dao
interface IAnswerDao {
    @Query("SELECT * FROM Answers WHERE questionId in (:questionIds)")
    suspend fun getAnswersByQuestionId(questionIds: Array<Int>): Array<AnswerEntity>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertAnswers(vararg answer: AnswerEntity)
}