package org.ptz.xakep.quizzes.framework.db

import androidx.room.*

@Entity(tableName = "Topics", foreignKeys = [ForeignKey(
    entity = LessonEntity::class,
    parentColumns = arrayOf("id"),
    childColumns = arrayOf("lessonId"),
    onDelete = ForeignKey.CASCADE
),
ForeignKey(
    entity = TopicNameEntity::class,
    parentColumns = arrayOf("id"),
    childColumns = arrayOf("topicNameId"),
    onDelete = ForeignKey.CASCADE
)], indices = [Index(value = ["topicNameId"]), Index(value = ["lessonId"])])
data class TopicEntity(
    @PrimaryKey(autoGenerate = true) val id: Int = 0,
    @ColumnInfo(name = "lessonId") val lessonId: Int,
    @ColumnInfo(name = "topicNameId") val topicNameId: Int
)