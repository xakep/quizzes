package org.ptz.xakep.quizzes

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import org.ptz.xakep.quizzes.presentation.stats.StatsViewModel
import org.koin.android.viewmodel.ext.android.viewModel
import org.ptz.xakep.quizzes.databinding.ActivityStatsBinding

class StatsActivity : AppCompatActivity() {

    private val viewModel: StatsViewModel by viewModel()
    private lateinit var binding: ActivityStatsBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityStatsBinding.inflate(layoutInflater)
        val view = binding.root
        setContentView(view)

        viewModel.progress.observe(this, Observer {
            binding.statsProgressTextView.text = "${it.solvedCount} / ${it.totalCount}"
            binding.statsProgressBar.progress = (it.solvedCount * 100) / it.totalCount
        })

        viewModel.getProgress()
    }
}