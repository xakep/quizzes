package org.ptz.xakep.quizzes

import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.LinearSnapHelper
import org.koin.android.ext.android.inject
import org.koin.android.viewmodel.ext.android.viewModel
import org.ptz.xakep.quizzes.databinding.ActivityQuizBinding
import org.ptz.xakep.quizzes.presentation.Status
import org.ptz.xakep.quizzes.presentation.models.QuizGivenAnswers
import org.ptz.xakep.quizzes.presentation.quiz.QuizAdapter
import org.ptz.xakep.quizzes.presentation.quiz.QuizViewModel

class QuizActivity : AppCompatActivity() {

    private val viewModel: QuizViewModel by viewModel()
    private val adapter: QuizAdapter by inject()
    private lateinit var binding: ActivityQuizBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityQuizBinding.inflate(layoutInflater)
        val view = binding.root
        setContentView(view)

        val snapHelper = LinearSnapHelper()
        snapHelper.attachToRecyclerView(binding.quizView)
        val horizontalLayoutManager = LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false)
        binding.quizView.layoutManager = horizontalLayoutManager
        binding.quizView.adapter = adapter

        binding.finishQuizBtn.setOnClickListener {
            val questions = adapter.currentList
            val answers = mutableListOf<Pair<Int, Int>>()
            questions.map {
                answers.add(Pair(it.id, it.selectedAnswer))
            }

            val link = intent.getStringExtra(getString(R.string.quiz_link_extra))
            val resultsIntent = Intent(this, ResultsActivity::class.java)

            val quizId = viewModel.state.value!!.data?.id
            resultsIntent.putExtra(getString(R.string.quiz_id_extra), quizId)
            resultsIntent.putExtra(
                getString(R.string.youtube_code_extra),
                intent.getStringExtra(getString(R.string.youtube_code_extra)))
            resultsIntent.putExtra(getString(
                R.string.quiz_link_extra), link)
            resultsIntent.putExtra(getString(R.string.given_answers), QuizGivenAnswers(answers))

            startActivity(resultsIntent)
            overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out)
        }

        viewModel.state.observe(this, Observer {
            when (it.status) {
                Status.PENDING -> {
                    val quizId = intent.getIntExtra(getString(R.string.quiz_id_extra), -1)
                    viewModel.load(quizId)
                }
                Status.SUCCESS -> {
                    adapter.submitList(it.data?.questions!!.toList())
                }
                else -> return@Observer
            }
        })

        viewModel.highlightAnswers.observe(this, Observer {
            adapter.setHighlightAnswers(it)
        })
    }

    override fun onNewIntent(intent: Intent?) {
        super.onNewIntent(intent)
        val showAnswers =
            intent?.getBooleanExtra(getString(R.string.show_correct_answers_extra), false)!!
        if (!showAnswers) {
            viewModel.setHighlightAnswersValue(false)
            adapter.resetAnswers()
        } else {
            viewModel.load(
                intent.getIntExtra(getString(R.string.quiz_id_extra), -1),
                intent.getParcelableExtra<QuizGivenAnswers>(getString(R.string.given_answers))!!.answers.toMap()
            )
        }

        binding.quizView.scrollToPosition(0)
    }

    fun enableFinishButton(enabled: Boolean) {
        binding.finishQuizBtn.isEnabled = enabled
    }
}
