package org.ptz.xakep.quizzes.presentation.lessons

import android.view.LayoutInflater
import android.view.View
import android.view.View.OnClickListener
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import org.ptz.xakep.quizzes.R
import org.ptz.xakep.quizzes.UIUtils
import org.ptz.xakep.quizzes.domain.ListLesson

class LessonsAdapter : ListAdapter<ListLesson, LessonsAdapter.ViewHolder>(ListLessonDiffCallback()) {
    companion object {
        lateinit var itemOnClickListener: OnClickListener
    }

    fun setOnClickListener(listener: OnClickListener) {
        itemOnClickListener = listener
    }

    inner class ViewHolder(view: View) : RecyclerView.ViewHolder(view), OnClickListener {
        init {
            view.setOnClickListener(this)
        }

        override fun onClick(v: View?) {
            itemOnClickListener.onClick(v)
        }

        val titleTextView: TextView = view.findViewById(R.id.lessonTitle)
        val avatar: ImageView = view.findViewById(R.id.avatar)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(
            LayoutInflater.from(parent.context)
                .inflate(R.layout.item_lesson, parent, false)
        )
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) = holder.run {
        val lesson = currentList[position]
        var title = lesson.title
        val a = "${lesson.avatar}?s=40"
        if (lesson.quizQuestionsCount != null) {
            val count = lesson.quizQuestionsCount!!
            val score = lesson.score
            val scoreTemplate = if (score != -1) "$score / " else ""
            title += if (count != 0) " ($scoreTemplate$count)" else " (no quiz)"
        }

        Glide.with(holder.itemView.context)
            .load("${holder.itemView.context.getString(R.string.gravatar_domain)}${a}")
            .into(avatar)

        holder.titleTextView.text = UIUtils.convertToHtml(title)
        holder.itemView.tag = lesson.id
    }
}