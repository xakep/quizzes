package org.ptz.xakep.quizzes.framework.db

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query

@Dao
interface IAttemptsDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertAttempt(attempt: AttemptEntity)

    @Query("SELECT highestScore FROM Attempts WHERE quizId = :quizId")
    suspend fun getScoreByQuizId(quizId: Int): Int?

    /*@Query("INSERT INTO Attempts VALUES(:quizId, :score) ON CONFLICT(quizId) DO UPDATE SET highestScore = (MAX(highestScore, :score))")
    suspend fun insertAttempt(quizId: Int, score: Int)*/
}