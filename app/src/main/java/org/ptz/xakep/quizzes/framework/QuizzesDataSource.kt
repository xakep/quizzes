package org.ptz.xakep.quizzes.framework

import org.ptz.xakep.quizzes.data.IQuizzesDataSource
import org.ptz.xakep.quizzes.domain.Answer
import org.ptz.xakep.quizzes.domain.CorrectAnswer
import org.ptz.xakep.quizzes.domain.Question
import org.ptz.xakep.quizzes.domain.Quiz
import org.ptz.xakep.quizzes.framework.db.AttemptEntity
import org.ptz.xakep.quizzes.framework.db.IAnswerDao
import org.ptz.xakep.quizzes.framework.db.IAttemptsDao
import org.ptz.xakep.quizzes.framework.db.IQuestionDao

class QuizzesDataSource(
    private val questionsDao: IQuestionDao,
    private val answersDao: IAnswerDao,
    private val attemptsDao: IAttemptsDao
) : IQuizzesDataSource {
    override suspend fun getQuizById(quizId: Int): Quiz {
        val questions = questionsDao.getQuestionsByQuizId(quizId)
        val questionIds = questions.map { it.id }
        val answers = answersDao
            .getAnswersByQuestionId(questionIds.toTypedArray())
            .groupBy { it.questionId }

        return Quiz(quizId, questions.map {
            Question(
                it.id,
                it.question,
                (answers[it.id] ?: listOf()).map { Answer(it.id, it.answer) }.toTypedArray(),
                it.correctAnswerId ?: -1,
            it.explanation ?: "")
        }.toTypedArray())
    }

    override suspend fun getCorrectAnswersByQuizId(quizId: Int): Array<CorrectAnswer> {
        return questionsDao.getCorrectAnswers(quizId).map {
            CorrectAnswer(it.id, it.correctAnswerId)
        }.toTypedArray()
    }

    override suspend fun setCorrectAnswersByQuestionId(
        questionId: Int,
        correctAnswerId: Int,
        explanation: String
    ) {
        questionsDao.updateQuestion(questionId, correctAnswerId, explanation)
    }

    override suspend fun recordAttempt(quizId: Int, score: Int) {
        attemptsDao.insertAttempt(AttemptEntity(quizId, score))
    }

    override suspend fun geScoreByQuizId(quizId: Int): Int {
        return attemptsDao.getScoreByQuizId(quizId) ?: 0
    }
}