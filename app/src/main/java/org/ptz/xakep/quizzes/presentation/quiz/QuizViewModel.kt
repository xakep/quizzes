package org.ptz.xakep.quizzes.presentation.quiz

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import kotlinx.coroutines.launch
import org.ptz.xakep.quizzes.domain.Quiz
import org.ptz.xakep.quizzes.interactors.GetQuizById
import org.ptz.xakep.quizzes.presentation.ViewModelEvent

class QuizViewModel(
    private val getQuizById: GetQuizById
) : ViewModel() {
    val state: MutableLiveData<ViewModelEvent<Quiz>> = MutableLiveData()
    val highlightAnswers: MutableLiveData<Boolean> = MutableLiveData()

    init {
        state.postValue(ViewModelEvent.pending())
    }

    fun load(quizId: Int) {
        viewModelScope.launch {
            highlightAnswers.postValue(false)
            state.postValue(ViewModelEvent.success(getQuizById(quizId)))
        }
    }

    fun load(quizId: Int, givenAnswers: Map<Int, Int>) {
        viewModelScope.launch {
            highlightAnswers.postValue(true)
            val quiz = getQuizById(quizId)
            quiz.questions.forEach {
                it.setAnswer(givenAnswers[it.id] ?: -1)
            }
            state.postValue(ViewModelEvent.success(quiz))
        }
    }

    fun setHighlightAnswersValue(value: Boolean) {
        highlightAnswers.postValue(value)
    }
}