package org.ptz.xakep.quizzes.presentation.lessons

import androidx.recyclerview.widget.DiffUtil
import org.ptz.xakep.quizzes.domain.ListLesson

class ListLessonDiffCallback: DiffUtil.ItemCallback<ListLesson>() {
    override fun areItemsTheSame(oldItem: ListLesson, newItem: ListLesson): Boolean {
        return oldItem.id == newItem.id
    }

    override fun areContentsTheSame(oldItem: ListLesson, newItem: ListLesson): Boolean {
        return oldItem == newItem
    }
}