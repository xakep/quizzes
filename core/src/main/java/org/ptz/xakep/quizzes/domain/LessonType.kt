package org.ptz.xakep.quizzes.domain

enum class LessonType {
    TODO, ATTEMPTED, SOLVED, NOQUIZ
}