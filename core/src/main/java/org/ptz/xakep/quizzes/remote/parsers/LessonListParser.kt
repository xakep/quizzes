package org.ptz.xakep.quizzes.remote.parsers

import ListLoadingProgress
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.flow.flowOn
import org.ptz.xakep.quizzes.domain.LatestLesson
import org.ptz.xakep.quizzes.domain.ListPageParsedLesson
import org.ptz.xakep.quizzes.domain.RefreshListResult
import org.ptz.xakep.quizzes.remote.model.Difficulty
import org.ptz.xakep.quizzes.remote.model.Teacher

class LessonListParser {
    fun parse(
        input: String,
        latestLesson: LatestLesson,
        topics: MutableMap<String, Int>,
        existingTeachers: List<Teacher>
    ): RefreshListResult {
        val newLessons = mutableListOf<ListPageParsedLesson>()
        var pos = 0
        val teachers = mutableMapOf<String, Teacher>()
        for (teacher in existingTeachers) {
            teachers[teacher.gravatar] = Teacher(teacher.teacherId, teacher.name, teacher.gravatar)
        }
        var newLessonId = latestLesson.lessonId
        while (true) {
            val start = input.indexOf("<div", pos)
            val end = input.indexOf("</div>", pos)
            val oneDiv = input.substring(start, end + 6)

            val lesson = parseOneLesson(
                oneDiv,
                -1,
                topics,
                teachers
            )

            pos = end + 6
            if (lesson == null) {
                continue
            } else {
                if (lesson.link == latestLesson.link) {
                    break
                }

                newLessons.add(lesson)
            }
        }

        // id is not auto generated in the DB
        // so, need to assign id in reversed order
        newLessonId += newLessons.size
        for (lesson in newLessons) {
            lesson.lessonId = newLessonId--
        }

        return RefreshListResult(newLessons, topics)
    }

    fun parse(input: String): Flow<Array<ListLoadingProgress>> {
        val count = getLessonsCount(input)
        var progress: Int
        var pos = input.length
        var current = 0
        val topics = mutableMapOf<String, Int>()
        val teachers = mutableMapOf<String, Teacher>()
        var currentNumber = 0

        val list = ArrayList<ListLoadingProgress>(100)

        return flow {
            emit(list.toTypedArray()) // emit 0 progress
            while (true) {
                val endOfDiv = input.lastIndexOf("</div>", pos)

                if (endOfDiv != -1) {
                    val beginOfDiv = input.lastIndexOf("<div", endOfDiv)
                    val oneDiv = input.substring(beginOfDiv, endOfDiv + 6)
                    val lesson = parseOneLesson(
                        oneDiv,
                        ++currentNumber,
                        topics,
                        teachers
                    )

                    if (lesson != null) {
                        progress = current * 100 / count + 1
                        list.add(ListLoadingProgress(lesson, progress, topics))
                        if (list.size == 100) {
                            emit(list.toTypedArray())
                            list.clear()
                        }
                    }

                    pos = beginOfDiv - 6
                    current++
                } else {
                    break
                }
            }

            if (list.size > 0) emit(list.toTypedArray())
        }
        .flowOn(Dispatchers.IO)
    }

    private fun getLessonsCount(s: String): Int {
        var pos = s.lastIndexOf("<script")
        val sub = s.substring(pos)
        val startPos = sub.indexOf("result_count")
        pos = sub.indexOf("=", startPos) + 1
        val number = sub.substring(pos, sub.indexOf(";", startPos)).trim { it <= ' ' }

        return Integer.valueOf(number) // might fail
    }

    private fun parseOneLesson(
        s: String,
        id: Int,
        topicNames: MutableMap<String, Int>,
        teachers: MutableMap<String, Teacher>
    ): ListPageParsedLesson? {
        var endPos: Int

        // 1. extract the link
        var startPos: Int = s.indexOf("'")
        if (startPos == -1) {
            // first tag - skip
            return null
        }

        endPos = s.indexOf("'", startPos + 1)
        val link = s.substring(s.lastIndexOf("/", endPos - 2) + 1, endPos - 1)

        // 2. teacher info
        // val teacherId: String? // gravatar
        startPos = s.indexOf("<img", endPos)
        val teacherInfoPos = startPos
        endPos = s.indexOf("/>", startPos + 1)

        val teacherId =
            parseTeacher(s, teacherInfoPos) ?: return null // returns null in case of resource
        if (!teachers.containsKey(teacherId)) {
            teachers[teacherId] = Teacher(teachers.size, "", teacherId)
        }
        val teacher: Teacher? = teachers[teacherId]

        // 3. title
        startPos = s.indexOf("<span", endPos)
        endPos = s.indexOf("</span>", startPos + 1)
        startPos = s.indexOf(">", startPos)
        val title = s.substring(startPos + 1, endPos)

        // 4. difficulty level and topics
        val difficultyAndTopics = getDifficultyAndTopics(s.substring(endPos), topicNames)

        return ListPageParsedLesson(
            id,
            link,
            title,
            difficultyAndTopics.first, // difficulty
            teacher!!, // TODO
            difficultyAndTopics.second
        ) // topics
    }

    private fun parseTeacher(s: String, startPos: Int): String? {
        val avatarPos = s.indexOf("avatar/", startPos)
        if (avatarPos == -1) return null // resource, not a lesson
        val start = avatarPos + 7

        val end = s.indexOf("?s=", start)
        return s.substring(start, end)
    }

    private fun getDifficultyAndTopics(
        data: String,
        topicNames: MutableMap<String, Int>
    ): Pair<Int, List<Int>> {
        val topics = mutableListOf<Int>()
        var startPos = 0
        var endPos: Int

        var level = 0 // difficulty level

        while (true) {
            startPos = data.indexOf("item\">", startPos) // len is 6
            if (startPos == -1) break

            endPos = data.indexOf("</", startPos)
            var s = data.substring(startPos + 6, endPos)
            // s could be:
            // 1-Beginner
            // &nbsp;&nbsp;&bull;&nbsp;&nbsp;2-Intermediate
            // anything else
            if (s.indexOf("nbsp;") != -1) {
                s = s.substring(s.lastIndexOf("nbsp;") + 5)
            }

            // topics
            if (s[1] != '-') {
                var topicId: Int
                if (!topicNames.containsKey(s)) {
                    topicId = topicNames.size + 1
                    topicNames[s] = topicId
                } else {
                    topicId = topicNames[s]!!
                }

                topics.add(topicId)
            } else {
                try {
                    level += when (val value = Integer.parseInt(s.substring(0, 1))) {
                        3 -> Difficulty.Advanced.value // advanced is 4
                        else -> value
                    }
                } catch (e: NumberFormatException) {
                    break
                }
            }

            startPos = endPos
        }

        return Pair(level, topics)
    }
}