package org.ptz.xakep.quizzes.interactors

import org.ptz.xakep.quizzes.data.QuizzesRepository

class GetQuizById(private val repository: QuizzesRepository) {
    suspend operator fun invoke(quizId: Int) = repository.getQuizById(quizId)
}