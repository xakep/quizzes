package org.ptz.xakep.quizzes.remote.model

class Question(
    val id: Int,
    val text: String,
    val answers: Array<Answer>
)