package org.ptz.xakep.quizzes.domain

class Quiz(
    val id: Int,
    val questions: Array<Question>
)
