package org.ptz.xakep.quizzes.interactors

import org.ptz.xakep.quizzes.data.LessonsRepository

class MarkAsSolved(private val repository: LessonsRepository) {
    suspend operator fun invoke(quizId: Int) = repository.markAsSolved(quizId)
}