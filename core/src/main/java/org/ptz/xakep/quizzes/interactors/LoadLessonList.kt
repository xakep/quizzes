package org.ptz.xakep.quizzes.interactors

import org.ptz.xakep.quizzes.remote.services.LessonListService

class LoadLessonList(private val lessonsService: LessonListService) {
    suspend operator fun invoke(listUrl: String) = lessonsService.loadLessonList(listUrl)
}