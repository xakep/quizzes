package org.ptz.xakep.quizzes.interactors

import org.ptz.xakep.quizzes.data.LessonsRepository

class GetLessonsCount(private val repository: LessonsRepository) {
    suspend operator fun invoke() = repository.getLessonsCount()
}