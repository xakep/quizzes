package org.ptz.xakep.quizzes.domain

class Filter(
    val lessonType: LessonType = LessonType.TODO,
    val search: String = "",
    val difficulty: Int = 0
) {
    companion object {
        val Default: Filter = Filter()
    }
}