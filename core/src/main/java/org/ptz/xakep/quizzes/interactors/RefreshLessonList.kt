package org.ptz.xakep.quizzes.interactors

import org.ptz.xakep.quizzes.remote.services.LessonListService

class RefreshLessonList(private val lessonsService: LessonListService) {
    suspend operator fun invoke(listUrl: String): Int = lessonsService.refreshLessonList(listUrl)
}