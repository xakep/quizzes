package org.ptz.xakep.quizzes.remote.parsers

import org.ptz.xakep.quizzes.remote.model.CorrectAnswer
import org.ptz.xakep.quizzes.remote.model.CorrectAnswers

class QuizResultsParser(

) {
    fun parse(input: String): CorrectAnswers {
        val area = extractQuizResultsArea(input)
        val result = parseResultsArea(area)

        return CorrectAnswers(result)
    }

    private fun extractQuizResultsArea(data: String): String {
        val startPos = data.indexOf("quiz_body")
        if (startPos == -1) {
            return ""
        }

        val endPos = data.indexOf("<script>", startPos)
        return data.substring(startPos, endPos)
    }

    private fun parseResultsArea(area: String): List<CorrectAnswer> {
        val result: MutableList<CorrectAnswer> = mutableListOf()
        var startPos = 0
        var endPos: Int
        var c = 0
        while (true) {
            startPos = area.indexOf("show-question", startPos)
            if (startPos == -1) {
                break
            }

            endPos = area.indexOf("</p>", startPos)
            val q = area.substring(startPos, endPos)
            result.add(parseQuestion(q))
            startPos = endPos
        }

        return result
    }

    // returns [correct answer, user's answer], can be equal
    private fun parseQuestion(question: String): CorrectAnswer {
        val result = intArrayOf(-1, -1) // correct, user's
        var startPos = 0
        var endPos: Int
        var c = 0
        while (true) {
            startPos = question.indexOf("<li", startPos)
            if (startPos == -1) {
                break
            }

            endPos = question.indexOf("</li>", startPos)
            val a = question.substring(startPos, endPos)
            if (a.indexOf("correct-answer") != -1) {
                result[0] = c
            }

            if (a.indexOf("user-answer") != -1) {
                result[1] = c
            }

            if (result[0] != -1 && result[1] != -1) {
                break
            }

            c++
            startPos = endPos
        }

        startPos = question.indexOf("ion'>", startPos)
        val explanation: String = question.substring(startPos + 5).trim()

        return CorrectAnswer(result[0], result[1], explanation)
    }
}