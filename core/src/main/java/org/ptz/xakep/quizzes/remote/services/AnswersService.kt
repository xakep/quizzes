package org.ptz.xakep.quizzes.remote.services

import org.ptz.xakep.quizzes.data.QuizzesRepository
import org.ptz.xakep.quizzes.domain.CorrectAnswer
import org.ptz.xakep.quizzes.remote.INetworkService
import org.ptz.xakep.quizzes.remote.parsers.QuizResultsParser

class AnswersService(
    private val networkService: INetworkService,
    private val parser: QuizResultsParser,
    private val repository: QuizzesRepository
) {
    suspend fun loadAnswers(quizId: Int, link: String, answers: List<Pair<Int, Int>>): Array<CorrectAnswer> {
        val fields = mutableMapOf<String, String>()
        answers.map {
            fields["question_id[]"] = it.first.toString() // first is a questionId
            fields["answer-${it.first}"] = it.second.toString() // seconds is a given answerId
        }

        val response: String
        try {
            response = networkService.postAnswers(link, quizId = quizId.toString(), fields = fields)
                .await()
        } catch (e: Exception) {
            return arrayOf() // error
        }
        val parsedAnswers = parser.parse(response)

        // map response to real ids
        val correctAnswerIds = mutableListOf<CorrectAnswer>()
        parsedAnswers.answers.forEachIndexed { index, element ->
            val correctAnswer = CorrectAnswer(
                answers[index].first, // questionId
                answers[index].second + (element.correctAnswerId - element.userGivenId)) // correctAnswerId
            correctAnswerIds.add(correctAnswer)

            repository.setCorrectAnswersByQuestionId(
                correctAnswer.questionId,
                correctAnswer.correctAnswerId!!,
                element.explanation
            )
        }

        return correctAnswerIds.toTypedArray()
    }
}