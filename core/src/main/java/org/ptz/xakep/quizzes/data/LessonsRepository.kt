package org.ptz.xakep.quizzes.data

import org.ptz.xakep.quizzes.domain.Filter
import org.ptz.xakep.quizzes.domain.LatestLesson
import org.ptz.xakep.quizzes.domain.ListPageParsedLesson
import org.ptz.xakep.quizzes.domain.Progress
import org.ptz.xakep.quizzes.remote.model.Lesson
import org.ptz.xakep.quizzes.remote.model.Teacher
import org.ptz.xakep.quizzes.remote.model.TopicName

class LessonsRepository(private val lessonsDataSource: ILessonsDataSource) {
    suspend fun getLessons(filter: Filter) = lessonsDataSource.getLessons(filter)

    suspend fun getLessonsCount() = lessonsDataSource.getLessonsCount()

    suspend fun getLessonById(id: Int) = lessonsDataSource.getLessonById(id)

    fun insertLessons(topicNames: List<TopicName>, vararg lessons: ListPageParsedLesson) =
        lessonsDataSource.insertLessons(topicNames, *lessons)

    suspend fun updateLesson(lessonId: Int, lesson: Lesson, gravatar: String?) =
        lessonsDataSource.updateLesson(lessonId, lesson, gravatar)

    suspend fun markAsSolved(quizId: Int) = lessonsDataSource.markAsSolved(quizId)

    suspend fun getProgress(): Progress = lessonsDataSource.getProgress()

    suspend fun getLatestLesson(): LatestLesson = lessonsDataSource.getLatestLesson()

    suspend fun getTopicNames(): List<TopicName> = lessonsDataSource.getTopicNames()

    suspend fun getTeachers(): List<Teacher> = lessonsDataSource.getTeachers()
}