package org.ptz.xakep.quizzes.remote.model

class Lesson(val quizId: Int? = null) {
    lateinit var youtubeCode: String
    lateinit var description: String
    lateinit var teacherName: String
    var questions: Array<Question> = arrayOf()
}