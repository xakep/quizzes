package org.ptz.xakep.quizzes.interactors

import org.ptz.xakep.quizzes.data.LessonsRepository

class GetLessonById(private val repository: LessonsRepository) {
    suspend operator fun invoke(id: Int) = repository.getLessonById(id)
}