package org.ptz.xakep.quizzes.domain

class RefreshListResult(
    val lessons: List<ListPageParsedLesson>,
    val topicNames: Map<String, Int>
)