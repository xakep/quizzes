package org.ptz.xakep.quizzes.domain

class CorrectAnswer(
    val questionId: Int,
    val correctAnswerId: Int?
)