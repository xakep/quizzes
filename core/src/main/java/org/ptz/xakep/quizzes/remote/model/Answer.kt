package org.ptz.xakep.quizzes.remote.model

class Answer(
    val id: Int,
    val text: String
)