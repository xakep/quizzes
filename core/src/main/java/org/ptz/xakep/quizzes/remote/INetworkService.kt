package org.ptz.xakep.quizzes.remote

import kotlinx.coroutines.Deferred
import retrofit2.http.*

interface INetworkService {
    @GET()
    fun getPageAsync(
        @Url url: String
    ): Deferred<String>

    @FormUrlEncoded
    @POST
    fun postAnswers(
        @Url url: String,
        @Field("action") action: String = "Finish+Quiz",
        @Field("quiz_id") quizId: String,
        @FieldMap fields: Map<String, String>
    ): Deferred<String>
}