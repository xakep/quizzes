package org.ptz.xakep.quizzes.domain

data class Question(
    val id: Int,
    val text: String,
    val answers: Array<Answer>,
    val correctAnswerId: Int = -1,
    val explanation: String = ""
) {
    var selectedAnswer: Int = -1
    fun setAnswer(selected: Int) {
        selectedAnswer = selected
    }
}