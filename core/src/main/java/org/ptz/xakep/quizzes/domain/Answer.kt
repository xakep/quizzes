package org.ptz.xakep.quizzes.domain

class Answer(
    val id: Int,
    val text: String
)