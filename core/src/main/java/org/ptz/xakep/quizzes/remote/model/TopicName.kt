package org.ptz.xakep.quizzes.remote.model

class TopicName(val id: Int, val name: String)