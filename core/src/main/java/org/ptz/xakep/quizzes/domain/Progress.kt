package org.ptz.xakep.quizzes.domain

class Progress(
    val solvedCount: Int,
    val totalCount: Int
)