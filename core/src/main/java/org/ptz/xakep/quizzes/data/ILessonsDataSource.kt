package org.ptz.xakep.quizzes.data

import org.ptz.xakep.quizzes.domain.*
import org.ptz.xakep.quizzes.remote.model.Teacher
import org.ptz.xakep.quizzes.remote.model.TopicName

interface ILessonsDataSource {
    suspend fun getLessons(filter: Filter) : List<ListLesson>

    suspend fun getLessonsCount(): Int

    suspend fun getLessonById(id: Int): Lesson

    fun insertLessons(topicNames: List<TopicName>, vararg listPageParsedLessons: ListPageParsedLesson)

    suspend fun updateLesson(lessonId: Int, lesson: org.ptz.xakep.quizzes.remote.model.Lesson, gravatar: String?)

    suspend fun markAsSolved(quizId: Int)

    suspend fun getProgress(): Progress

    suspend fun getLatestLesson(): LatestLesson

    suspend fun getTopicNames(): List<TopicName>

    suspend fun getTeachers(): List<Teacher>
}