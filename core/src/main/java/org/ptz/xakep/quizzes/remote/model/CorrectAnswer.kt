package org.ptz.xakep.quizzes.remote.model

class CorrectAnswer(
    val correctAnswerId: Int,
    val userGivenId: Int,
    val explanation: String
)