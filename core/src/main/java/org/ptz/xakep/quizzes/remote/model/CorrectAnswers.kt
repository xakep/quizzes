package org.ptz.xakep.quizzes.remote.model

class CorrectAnswers(val answers: List<CorrectAnswer>)