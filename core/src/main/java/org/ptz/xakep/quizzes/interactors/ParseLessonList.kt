package org.ptz.xakep.quizzes.interactors

import org.ptz.xakep.quizzes.remote.services.LessonListService

class ParseLessonList(private val lessonsService: LessonListService) {
    operator fun invoke(listUrl: String) = lessonsService.parseLessonList(listUrl)
}