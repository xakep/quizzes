package org.ptz.xakep.quizzes.data

import org.ptz.xakep.quizzes.domain.CorrectAnswer
import org.ptz.xakep.quizzes.domain.Quiz

interface IQuizzesDataSource {
    suspend fun getQuizById(quizId: Int): Quiz

    suspend fun getCorrectAnswersByQuizId(quizId: Int): Array<CorrectAnswer>

    suspend fun setCorrectAnswersByQuestionId(questionId: Int, correctAnswerId: Int, explanation: String)

    suspend fun recordAttempt(quizId: Int, score: Int)

    suspend fun geScoreByQuizId(quizId: Int): Int
}