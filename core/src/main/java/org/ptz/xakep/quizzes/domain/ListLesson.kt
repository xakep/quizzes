package org.ptz.xakep.quizzes.domain

data class ListLesson(
    val id: Int,
    val title: String,
    val quizQuestionsCount: Int?,
    val score: Int,
    val avatar: String
)