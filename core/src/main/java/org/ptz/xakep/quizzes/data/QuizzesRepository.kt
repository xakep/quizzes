package org.ptz.xakep.quizzes.data

class QuizzesRepository(private val quizzesDataSource: IQuizzesDataSource) {
    suspend fun getQuizById(quizId: Int) = quizzesDataSource.getQuizById(quizId)

    suspend fun getCorrectAnswersByQuizId(quizId: Int) = quizzesDataSource.getCorrectAnswersByQuizId(quizId)

    suspend fun setCorrectAnswersByQuestionId(
        questionId: Int,
        correctAnswerId: Int,
        explanation: String
    ) = quizzesDataSource.setCorrectAnswersByQuestionId(questionId, correctAnswerId, explanation)

    suspend fun recordAttempt(quizId: Int, score: Int) = quizzesDataSource.recordAttempt(quizId, score)

    suspend fun getScoreByQuizId(quizId: Int) = quizzesDataSource.geScoreByQuizId(quizId)
}