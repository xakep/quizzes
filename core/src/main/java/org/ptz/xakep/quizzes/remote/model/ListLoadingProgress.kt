import org.ptz.xakep.quizzes.domain.ListPageParsedLesson

class ListLoadingProgress(
    val lesson: ListPageParsedLesson,
    val progress: Int,
    val topicNames: Map<String, Int>
)