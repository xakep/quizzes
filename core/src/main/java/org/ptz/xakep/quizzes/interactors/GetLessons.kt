package org.ptz.xakep.quizzes.interactors

import org.ptz.xakep.quizzes.data.LessonsRepository
import org.ptz.xakep.quizzes.domain.Filter

class GetLessons(private val repository: LessonsRepository) {
    suspend operator fun invoke(filter: Filter) = repository.getLessons(filter)
}