package org.ptz.xakep.quizzes.remote.services

import org.ptz.xakep.quizzes.data.LessonsRepository
import org.ptz.xakep.quizzes.domain.Lesson
import org.ptz.xakep.quizzes.remote.INetworkService
import org.ptz.xakep.quizzes.remote.parsers.LessonParser

class LessonService(
    private val networkService: INetworkService,
    private val parser: LessonParser,
    private val repository: LessonsRepository
) {
    suspend fun loadLesson(lessonId: Int, link: String, gravatar: String?): Lesson? {
        val response: String
        try {
            response = networkService.getPageAsync(link).await()
        } catch (e: Exception) {
            return null
        }

        val lesson = parser.parse(response)
        repository.updateLesson(lessonId, lesson, gravatar)

        return repository.getLessonById(lessonId)
    }
}