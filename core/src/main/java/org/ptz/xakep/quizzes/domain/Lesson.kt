package org.ptz.xakep.quizzes.domain

class Lesson(
    val quizId: Int,
    val link: String,
    val youtube: String,
    val title: String,
    val difficulty: Int,
    val topics: List<String>,
    val description: String,
    val quizQuestionsCount: Int,
    val teacherName: String,
    val teacherGravatar: String
)