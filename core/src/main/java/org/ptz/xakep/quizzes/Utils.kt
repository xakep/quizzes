package org.ptz.xakep.quizzes

import okhttp3.ResponseBody
import java.io.BufferedReader
import java.io.InputStream
import java.io.InputStreamReader
import java.io.Reader

class Utils {
    companion object {
        fun responseToString(response: ResponseBody) : String {
            return inputStreamToString(response.byteStream())
        }

        private fun inputStreamToString(input: InputStream?) : String {
            if (input == null) return ""
            val buf = StringBuilder()
            val reader = BufferedReader(InputStreamReader(input, "UTF-8") as Reader)
            var str: String?

            while (true) {
                str = reader.readLine()
                if (str == null) break
                buf.append(str)
            }

            reader.close()

            return buf.toString()
        }

        // TODO: use flags
        fun getDifficulties(level: Int): IntArray {
            return when (level) {
                1 -> intArrayOf(0)
                2 -> intArrayOf(1)
                3 -> intArrayOf(0, 1)
                4 -> intArrayOf(2)
                6 -> intArrayOf(1, 2)
                7 -> intArrayOf(0, 1, 2)
                else -> IntArray(0)
            }
        }
    }
}