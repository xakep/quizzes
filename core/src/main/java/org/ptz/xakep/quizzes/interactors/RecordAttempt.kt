package org.ptz.xakep.quizzes.interactors

import org.ptz.xakep.quizzes.data.QuizzesRepository

class RecordAttempt(private val repository: QuizzesRepository) {
    suspend operator fun invoke(quizId: Int, score: Int) = repository.recordAttempt(quizId, score)
}