package org.ptz.xakep.quizzes.remote.services

import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.*
import org.ptz.xakep.quizzes.data.LessonsRepository
import org.ptz.xakep.quizzes.remote.INetworkService
import org.ptz.xakep.quizzes.remote.model.TopicName
import org.ptz.xakep.quizzes.remote.parsers.LessonListParser

class LessonListService(
    private val networkService: INetworkService,
    private val lessonListParser: LessonListParser,
    private val repository: LessonsRepository
) {
    suspend fun loadLessonList(listUrl: String): String {
        return try {
            networkService.getPageAsync(listUrl).await()
        } catch (e: Exception) {
            "" // TODO
        }
    }

    fun parseLessonList(response: String): Flow<Int> {
        val insertedTopics = HashSet<Int>()

        return lessonListParser.parse(response)
            .map { it ->
                if (it.isEmpty()) {
                    0
                } else {
                    val newTopicNames = it.last().topicNames.filter { topic ->
                        !insertedTopics.contains(topic.value)
                    }
                    val topicNames = newTopicNames.map { topic ->
                        TopicName(topic.value, topic.key)
                    }
                    insertedTopics.addAll(newTopicNames.map { topic -> topic.value })

                    // save to db
                    val list = it.map { item -> item.lesson }.toTypedArray()
                    repository.insertLessons(topicNames, *list) // bulk save

                    // emit progress
                    it.last().progress
                }
            }
            .flowOn(Dispatchers.IO)
    }

    suspend fun refreshLessonList(listUrl: String): Int {
        val response: String
        try {
            response = networkService.getPageAsync(listUrl).await()
        } catch (e: Exception) {
            return -1 // error
        }

        // get latest quiz code and topic names
        val latest = repository.getLatestLesson()
        val topicNames = mutableMapOf<String, Int>()
        repository.getTopicNames().map {
            topicNames.put(it.name, it.id)
        }
        val teachers = repository.getTeachers()

        // parse new quizzes only (from beginning of the page)
        val parseResult = lessonListParser.parse(response, latest, topicNames, teachers)
        val lessons = parseResult.lessons.toTypedArray()
        if (lessons.isNotEmpty()) {
            val topics = parseResult.topicNames.map { topic ->
                TopicName(topic.value, topic.key)
            }

            // save to db
            // TODO: split by 100
            repository.insertLessons(topics, *lessons) // bulk save
        }

        return lessons.size
    }
}