package org.ptz.xakep.quizzes.domain

class LatestLesson(
    val lessonId: Int,
    val link: String
)