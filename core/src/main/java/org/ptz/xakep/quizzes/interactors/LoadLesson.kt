package org.ptz.xakep.quizzes.interactors

import org.ptz.xakep.quizzes.remote.services.LessonService

class LoadLesson(private val lessonService: LessonService) {
    suspend operator fun invoke(lessonId: Int, link: String, gravatar: String?) =
        lessonService.loadLesson(lessonId, link, gravatar)
}