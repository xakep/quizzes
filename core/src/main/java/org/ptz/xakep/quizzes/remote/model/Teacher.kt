package org.ptz.xakep.quizzes.remote.model

data class Teacher(val teacherId: Int, val name: String, val gravatar: String)