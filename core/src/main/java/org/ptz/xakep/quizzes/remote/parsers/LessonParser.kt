package org.ptz.xakep.quizzes.remote.parsers

import org.ptz.xakep.quizzes.remote.model.Answer
import org.ptz.xakep.quizzes.remote.model.Lesson
import org.ptz.xakep.quizzes.remote.model.Question
import java.util.ArrayList

class LessonParser {
    fun parse(input: String): Lesson {
        val quizArea = extractQuizArea(input)
        val youtube = extractYoutubeCode(input)

        val lesson = if (quizArea == "") {
            // no quiz
            Lesson()
        } else {
            parseQuiz(quizArea)
        }

        lesson.youtubeCode = youtube
        lesson.description = extractLessonDescription(input)
        // extract teacher name
        lesson.teacherName = extractTeacherName(input)

        return lesson
    }

    private fun extractQuizArea(data: String): String {
        val startPos = data.indexOf("quiz-area")
        if (startPos == -1) {
            return ""
        }

        val endPos = data.indexOf("</form>", startPos)
        return data.substring(startPos + 12, endPos)
    }

    private fun extractYoutubeCode(data: String): String {
        val startPos = data.indexOf("/embed/") // len = 7
        if (startPos == -1) {
            return ""
        }

        val endPos = data.indexOf("\">", startPos)

        return data.substring(startPos + 7, endPos)
    }

    private fun extractTeacherName(data: String): String {
        var startPos = data.indexOf("avatar-80")
        if (startPos == -1) {
            return ""
        }

        startPos = data.indexOf("<br />", startPos)
        val endPos = data.indexOf("<", startPos + 1)

        return data.substring(startPos + 6, endPos).trim()
    }

    private fun extractLessonDescription(data: String): String {
        var s = data.indexOf("featured_description")
        if (s == -1) {
            return ""
        }
        s = data.indexOf(">", s) + 1
        val e = data.indexOf("</span>", s)
        val span = data.indexOf("<span>", s)
        if (span != -1 && span < e) {
            s = span + 6
        }

        return data.substring(s, e)
    }

    private fun parseQuiz(quizArea: String): Lesson {
        // 1. get quiz id
        var startPos = quizArea.indexOf("id=")
        var endPos = quizArea.indexOf(">", startPos)

        // 'id="quiz-' len is 9; '"' len is 1
        val quizId = Integer.valueOf(quizArea.substring(startPos + 9, endPos - 1))

        // 2. parse questions
        endPos++
        val list = ArrayList<Question>() // TODO: get rid of this
        while (true) {
            startPos = endPos
            endPos = quizArea.indexOf("</label></div>", startPos)
            if (endPos == -1) {
                break
            }
            val oneQuestion = quizArea.substring(startPos, endPos)
            val question = parseOneQuestion(oneQuestion)
            list.add(question)
            endPos += 14 // "</label></div>".len
        }
        val lesson = Lesson(quizId)
        lesson.questions = list.toTypedArray()

        return lesson
    }

    private fun parseOneQuestion(data: String): Question {
        // 1. extract content
        var startPos = data.indexOf("content")
        var endPos = data.indexOf("</div>", startPos)
        var content = data.substring(startPos + 9, endPos)
        content = replaceAllBlanks(content)

        // 2. extract id
        startPos = data.indexOf("value='")
        endPos = data.indexOf("/>", startPos)
        val id = Integer.valueOf(data.substring(startPos + 7, endPos - 2))

        // 3. extract answers
        endPos += 2
        val answersList = ArrayList<Answer>()
        while (true) {
            startPos = endPos
            endPos = data.indexOf("</span>", startPos)
            if (endPos == -1) {
                break
            }
            val oneAnswer = data.substring(startPos, endPos)
            val answer = parseOneAnswer(oneAnswer)
            answersList.add(answer)
            endPos += 7 // "</span>".len
        }

        return Question(id, content.trim(), answersList.toTypedArray())
    }

    private fun replaceAllBlanks(content: String): String {
        var s = 0
        val sb: StringBuilder

        s = content.indexOf("_")
        if (s == -1) {
            return content
        } else {
            sb = StringBuilder()
            sb.append(content.substring(0, s + 1))
        }

        for (i in s + 1 until content.length) {
            val c = content[i]
            if (c != '_') {
                sb.append(c)
            } else {
                if (sb[sb.length - 1] != '_') {
                    sb.append(c)
                }
            }
        }

        return sb.toString()
    }

    private fun parseOneAnswer(data: String): Answer {
        // 1. get id
        var startPos = data.indexOf("answer-id")
        val endPos = data.indexOf("'", startPos)
        val id = Integer.valueOf(data.substring(startPos + 10, endPos))

        // 2. get text
        startPos = data.indexOf("<span>", endPos)
        val text = data.substring(startPos + 6, data.length)

        return Answer(id, text.trim())
    }
}