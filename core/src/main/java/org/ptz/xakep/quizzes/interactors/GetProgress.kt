package org.ptz.xakep.quizzes.interactors

import org.ptz.xakep.quizzes.data.LessonsRepository

class GetProgress(private val repository: LessonsRepository) {
    suspend operator fun invoke() = repository.getProgress()
}