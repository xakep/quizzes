package org.ptz.xakep.quizzes.remote

import com.jakewharton.retrofit2.adapter.kotlin.coroutines.CoroutineCallAdapterFactory
import retrofit2.Retrofit

class NetworkService() {
    companion object {
        fun getInstance(domainUrl: String): INetworkService {
            return Retrofit.Builder()
                .baseUrl(domainUrl)
                .addConverterFactory(CustomConverterFactory())
                .addCallAdapterFactory(CoroutineCallAdapterFactory())
                .build()
                .create(INetworkService::class.java)
        }
    }
}