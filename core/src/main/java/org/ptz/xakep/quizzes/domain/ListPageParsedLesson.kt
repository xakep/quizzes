package org.ptz.xakep.quizzes.domain

import org.ptz.xakep.quizzes.remote.model.Teacher

class ListPageParsedLesson(
    var lessonId: Int,
    val link: String,
    val title: String,
    val difficulty: Int,
    val teacher: Teacher,
    val topics: List<Int>)