package org.ptz.xakep.quizzes.interactors

import org.ptz.xakep.quizzes.remote.services.AnswersService

class LoadCorrectAnswers(private val answersService: AnswersService) {
    suspend operator fun invoke(quizId: Int, link: String, fields: List<Pair<Int, Int>>) =
        answersService.loadAnswers(quizId, link, fields)
}