package org.ptz.xakep.quizzes.interactors

import org.ptz.xakep.quizzes.data.QuizzesRepository

class GetScoreByQuizId(private val repository: QuizzesRepository) {
    suspend operator fun invoke(quizId: Int): Int = repository.getScoreByQuizId(quizId)
}