package org.ptz.xakep.quizzes.remote.model

enum class Difficulty(val value: Int) {
    Beginner(1),
    Intermediate(2),
    Advanced(4)
}